*** Settings ***
Documentation    Test case sur Cura Login 
Library    SeleniumLibrary
Resource    resource2.resource
Test Teardown    Close Browser

*** Test Cases ***
Test1 
    [Tags]    tag1
    
    Given je suis connecté à la page de rdv 
    When je renseigne les informations obligatoires dont Healthcare    ${JDD1.health}
    And Je soumets le formulaire
    Then j'ai une confirmation de rdv    ${JDD1.check_healthcare}
    And je reviens à la page d'accueil

Test2
    [Tags]    tag2
    Given je suis connecté à la page de rdv 
    When je renseigne les informations obligatoires dont Healthcare    ${JDD2.health}
    And Je soumets le formulaire
    Then j'ai une confirmation de rdv    ${JDD2.check_healthcare}
    And je reviens à la page d'accueil

Test3
    [Tags]    tag3
    Given je suis connecté à la page de rdv 
    When je renseigne les informations obligatoires dont Healthcare    ${JDD3.health}
    And Je soumets le formulaire
    Then j'ai une confirmation de rdv    ${JDD3.check_healthcare}
    And je reviens à la page d'accueil